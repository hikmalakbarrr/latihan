#include <stdio.h>

int factorial(int n){
    int f = 1;
    for(int i=1; i<=n; i++){
        f*=i; 
    }
    return f;
}

int combination(int n, int r){
    return factorial(n) / (factorial(n-r) * factorial(r));
}

int main(){
    
    int n, i, j, k;

    printf("Masukan N : ");
    scanf("%d", &n);

    for(i=0; i<n; i++){
        
        for(j=0; j<=n-i; j++){
            printf(" ");
        }

        for(k=0; k<=i; k++){
            
            if(k==0 || k==i){
                printf("%d ", combination(i,k)); 
            }
            else {
                printf("%d ", combination(i,k));
            }
        }
        printf("\n");
    }
    return 0;
}
